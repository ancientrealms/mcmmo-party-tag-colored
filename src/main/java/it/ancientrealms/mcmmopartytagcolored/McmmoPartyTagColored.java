package it.ancientrealms.mcmmopartytagcolored;

import org.apache.commons.lang.StringUtils;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Team;

import com.gmail.nossr50.api.PartyAPI;

import it.ancientrealms.mcmmopartytagcolored.listener.Listener;
import net.kyori.adventure.text.format.NamedTextColor;

public final class McmmoPartyTagColored extends JavaPlugin
{
    private static McmmoPartyTagColored instance;

    @Override
    public void onEnable()
    {
        instance = this;

        this.getServer().getPluginManager().registerEvents(new Listener(), this);

        PartyAPI.getParties().forEach(p -> {
            final String teamname = StringUtils.left("party-" + p.getName(), 16);

            if (this.getServer().getScoreboardManager().getMainScoreboard().getTeams().stream().anyMatch(t -> t.getName().equals(teamname)))
            {
                return;
            }

            final Team team = this.getServer().getScoreboardManager().getMainScoreboard().registerNewTeam(teamname);
            team.color(NamedTextColor.GREEN);

            p.getMembers().keySet().forEach(uuid -> team.addEntry(this.getServer().getOfflinePlayer(uuid).getName()));
        });
    }

    @Override
    public void onDisable()
    {
    }

    public static McmmoPartyTagColored getInstance()
    {
        return instance;
    }
}
