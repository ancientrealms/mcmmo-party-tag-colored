package it.ancientrealms.mcmmopartytagcolored.listener;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;

import com.gmail.nossr50.api.PartyAPI;
import com.gmail.nossr50.events.party.McMMOPartyChangeEvent;
import com.gmail.nossr50.util.player.UserManager;

import it.ancientrealms.mcmmopartytagcolored.McmmoPartyTagColored;
import net.kyori.adventure.text.format.NamedTextColor;

public final class Listener implements org.bukkit.event.Listener
{
    private final McmmoPartyTagColored plugin = McmmoPartyTagColored.getInstance();
    private final Map<UUID, BukkitRunnable> tasks = new HashMap<>();

    @EventHandler
    public void playerJoin(PlayerJoinEvent event)
    {
        final BukkitRunnable task = new BukkitRunnable() {
            @Override
            public void run()
            {
                // mcmmo player not yet loaded
                if (UserManager.getPlayer(event.getPlayer()) == null)
                {
                    return;
                }

                if (PartyAPI.inParty(event.getPlayer()))
                {
                    this.cancel();
                    return;
                }

                for (Team t : plugin.getServer().getScoreboardManager().getMainScoreboard().getTeams())
                {
                    if (t.getEntries().contains(event.getPlayer().getName()))
                    {
                        t.removeEntry(event.getPlayer().getName());
                        break;
                    }
                }

                this.cancel();
            }
        };

        task.runTaskTimer(plugin, 0, 20);
        tasks.put(event.getPlayer().getUniqueId(), task);
    }

    @EventHandler
    public void playerQuit(PlayerQuitEvent event)
    {
        final BukkitRunnable task = tasks.remove(event.getPlayer().getUniqueId());

        if (task == null || task.isCancelled())
        {
            return;
        }

        task.cancel();
    }

    @EventHandler
    public void partyDisappeared(McMMOPartyChangeEvent event)
    {
        final boolean flag = event.getReason().equals(McMMOPartyChangeEvent.EventReason.LEFT_PARTY)
                || event.getReason().equals(McMMOPartyChangeEvent.EventReason.KICKED_FROM_PARTY)
                || event.getReason().equals(McMMOPartyChangeEvent.EventReason.DISBANDED_PARTY);

        if (!flag)
        {
            return;
        }

        final Team playerteam = plugin.getServer()
                .getScoreboardManager()
                .getMainScoreboard()
                .getTeam(StringUtils.left("party-" + event.getOldParty(), 16));

        if (playerteam == null)
        {
            return;
        }

        if (event.getReason().equals(McMMOPartyChangeEvent.EventReason.DISBANDED_PARTY))
        {
            playerteam.unregister();
            return;
        }

        playerteam.removeEntry(event.getPlayer().getName());
    }

    @EventHandler
    public void partyJoined(McMMOPartyChangeEvent event)
    {
        if (!event.getReason().equals(McMMOPartyChangeEvent.EventReason.JOINED_PARTY))
        {
            return;
        }

        final Team team = plugin.getServer()
                .getScoreboardManager()
                .getMainScoreboard()
                .getTeam(StringUtils.left("party-" + event.getNewParty(), 16));

        if (team == null)
        {
            return;
        }

        team.addEntry(event.getPlayer().getName());
    }

    @EventHandler
    public void partyCreated(McMMOPartyChangeEvent event)
    {
        if (!event.getReason().equals(McMMOPartyChangeEvent.EventReason.CREATED_PARTY))
        {
            return;
        }

        final Team team = plugin.getServer()
                .getScoreboardManager()
                .getMainScoreboard()
                .registerNewTeam(StringUtils.left("party-" + event.getNewParty(), 16));

        team.color(NamedTextColor.GREEN);
        team.addEntry(event.getPlayer().getName());
    }

    @EventHandler
    public void partyRenamed(McMMOPartyChangeEvent event)
    {
        if (!event.getReason().equals(McMMOPartyChangeEvent.EventReason.CHANGED_PARTIES))
        {
            return;
        }

        if (event.getOldParty() == null)
        {
            return;
        }

        if (event.getOldParty().equalsIgnoreCase(event.getNewParty()))
        {
            return;
        }

        final String oldteamname = StringUtils.left("party-" + event.getOldParty(), 16);
        final String newteamname = StringUtils.left("party-" + event.getNewParty(), 16);

        final Team oldteam = plugin.getServer()
                .getScoreboardManager()
                .getMainScoreboard()
                .getTeam(oldteamname);
        final Team newteam = plugin.getServer()
                .getScoreboardManager()
                .getMainScoreboard()
                .registerNewTeam(newteamname);

        oldteam.getEntries().forEach(newteam::addEntry);
        newteam.color(NamedTextColor.GREEN);
        oldteam.unregister();
    }
}
